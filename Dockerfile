# Cloud Build Sample
# Dockerfile to create a sample image
FROM nginx:1.23.0-alpine

LABEL version="1.2.3" \
      description="Cloud Build Sample"

# We need to give all user groups write permission on folder /var/cache/nginx/ and file /var/run/nginx.pid.
# So users with random uid will be able to run NGINX.
RUN chmod -R a+w /var/cache/nginx/ \
        && touch /var/run/nginx.pid \
        && chmod a+w /var/run/nginx.pid \
        && rm /etc/nginx/conf.d/*

COPY nginx.conf /etc/nginx/conf.d/
COPY dist/ /usr/share/nginx/html/

EXPOSE 8080
USER nginx
