VERSION=1.2.3
PROJECT=cloud-build
APP=sample
TAG=$(VERSION)

all: build save run
test: build run

build:
	@echo "Building image:"
	docker build -t $(PROJECT)/$(APP):$(TAG) .

save:
	@echo "Saving image:"
	docker save -o $(PROJECT)-$(APP).$(TAG).tar $(PROJECT)/$(APP):$(TAG)

run:
	@echo "Running container:"
	docker run --rm -p 8080:8080 $(PROJECT)/$(APP):$(TAG)
